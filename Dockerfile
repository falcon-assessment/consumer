FROM openjdk:12-alpine

WORKDIR /app

COPY target/consumer.jar .

ENTRYPOINT exec java $JAVA_OPTS -jar consumer.jar