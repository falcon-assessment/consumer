package com.falcon.consumer;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.document.RawJsonDocument;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.data.couchbase.core.CouchbaseTemplate;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class JsonMessageListenerTest
{
		private JsonMessageListener listener;

		private AmqpAdmin admin;

		private Binding binding;

		private CouchbaseTemplate template;

		private ArgumentCaptor<RawJsonDocument> documentCaptor;

		@Before
		public void init()
		{
				admin = mock(AmqpAdmin.class);
				binding = mock(Binding.class);
				template = mock(CouchbaseTemplate.class);
				listener = new JsonMessageListener(admin, binding, template);
				documentCaptor = ArgumentCaptor.forClass(RawJsonDocument.class);

				when(template.getCouchbaseBucket()).thenReturn(mock(Bucket.class));
		}

		@Test
		public void onMessageInsertTest()
		{
				String body = "{\"test\":\"test\"}";
				Message message = new Message(body.getBytes(), new MessageProperties());

				listener.onMessage(message);

				verify(template.getCouchbaseBucket(), times(1))
						.insert(documentCaptor.capture());
				RawJsonDocument actual = documentCaptor.getValue();

				assertNotNull(UUID.fromString(actual.id()));
				assertEquals(body, actual.content());
		}

		@Test
		public void declareBindingTest()
		{
				verify(admin, times(1)).declareBinding(binding);
		}
}
