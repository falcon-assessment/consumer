package com.falcon.rest;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.view.ViewQuery;
import com.couchbase.client.java.view.ViewResult;
import com.falcon.consumer.View;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.data.couchbase.core.CouchbaseTemplate;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ConsumerControllerTest
{
		private ConsumerController controller;

		private CouchbaseTemplate template;

		private ArgumentCaptor<ViewQuery> viewQueryCaptor;

		@Before
		public void init()
		{
				template = mock(CouchbaseTemplate.class);
				controller = new ConsumerController(template);
				viewQueryCaptor = ArgumentCaptor.forClass(ViewQuery.class);

				Bucket bucket = mock(Bucket.class);
				when(bucket.query(any(ViewQuery.class))).thenReturn(mock(ViewResult.class));
				when(template.getCouchbaseBucket()).thenReturn(bucket);
		}

		@Test
		public void getAllUsesViewAllTest()
		{
				controller.getAll();

				verify(template.getCouchbaseBucket(), times(1))
						.query(viewQueryCaptor.capture());

				ViewQuery actual = viewQueryCaptor.getValue();
				assertEquals(View.VIEW_ALL.getDesign(), actual.getDesign());
				assertEquals(View.VIEW_ALL.getView(), actual.getView());
		}

		@Test
		public void getAllReturnsAllRowsTest()
		{
				controller.getAll();
				verify(template.getCouchbaseBucket().query(mock(ViewQuery.class)), times(1))
						.allRows();
		}

}
