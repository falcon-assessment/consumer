package com.falcon.rest;

import com.couchbase.client.java.document.RawJsonDocument;
import com.couchbase.client.java.view.Stale;
import com.couchbase.client.java.view.ViewQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.couchbase.core.CouchbaseTemplate;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static com.falcon.consumer.View.VIEW_ALL;

@RestController("/data")
public class ConsumerController
{
		private final CouchbaseTemplate template;

		@Autowired
		public ConsumerController(CouchbaseTemplate template)
		{
				this.template = template;
		}

		@GetMapping(value = "/json/all", produces = MediaType.APPLICATION_JSON_VALUE)
		public List<RawJsonDocument> getAll()
		{
				return template
						.getCouchbaseBucket()
						.query(ViewQuery.from(VIEW_ALL.getDesign(), VIEW_ALL.getView()).stale(Stale.FALSE))
						.allRows()
						.stream()
						.map(v -> v.document(RawJsonDocument.class))
						.collect(Collectors.toList());
		}
}
