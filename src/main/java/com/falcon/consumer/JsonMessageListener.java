package com.falcon.consumer;

import com.couchbase.client.java.document.RawJsonDocument;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.couchbase.core.CouchbaseTemplate;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class JsonMessageListener implements MessageListener
{
		private final CouchbaseTemplate template;

		@Autowired
		public JsonMessageListener(AmqpAdmin admin, Binding binding, CouchbaseTemplate template)
		{
				this.template = template;
				admin.declareBinding(binding);
		}

		public void onMessage(Message message)
		{
				String body = new String(message.getBody());
				template
						.getCouchbaseBucket()
						.insert(RawJsonDocument.create(UUID.randomUUID().toString(), body));
		}
}
