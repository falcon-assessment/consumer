package com.falcon.consumer;

public enum View
{
		VIEW_ALL("jsonData", "all");

		private final String design;

		private final String view;

		View(String design, String view)
		{
				this.design = design;
				this.view = view;
		}

		public String getDesign()
		{
				return design;
		}

		public String getView()
		{
				return view;
		}
}
