package com.falcon.config;

import com.couchbase.client.java.document.RawJsonDocument;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class RawJsonDocumentSerializer extends StdSerializer<RawJsonDocument>
{
		public RawJsonDocumentSerializer()
		{
				this(null);
		}

		protected RawJsonDocumentSerializer(Class<RawJsonDocument> t)
		{
				super(t);
		}

		@Override
		public void serialize(RawJsonDocument rawJsonDocument, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException
		{
				jsonGenerator.writeStartObject();
				jsonGenerator.writeFieldName("id");
				jsonGenerator.writeString(rawJsonDocument.id());
				jsonGenerator.writeFieldName("content");
				jsonGenerator.writeRawValue(rawJsonDocument.content());
				jsonGenerator.writeEndObject();
		}
}
