package com.falcon.config;

import com.falcon.consumer.JsonMessageListener;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessagingConfig
{
		private final String exchangeName;

		private final String queueName;

		private final String routingKey;

		@Autowired
		public MessagingConfig(@Value("${spring.rabbitmq.queues.consumer.exchangeName}") String exchangeName,
		                       @Value("${spring.rabbitmq.queues.consumer.queueName}") String queueName,
		                       @Value("${spring.rabbitmq.queues.consumer.routingKey}") String routingKey)
		{
				this.exchangeName = exchangeName;
				this.queueName = queueName;
				this.routingKey = routingKey;
		}

		@Bean
		public DirectExchange producerExchange()
		{
				return new DirectExchange(exchangeName);
		}

		@Bean
		public Queue consumerQueue()
		{
				return new Queue(queueName);
		}

		@Bean
		public Binding binding()
		{

				return BindingBuilder
						.bind(consumerQueue())
						.to(producerExchange())
						.with(routingKey);
		}

		@Bean
		@Autowired
		public SimpleMessageListenerContainer listenerContainer(ConnectionFactory connectionFactory,
		                                                        JsonMessageListener listener
		)
		{
				SimpleMessageListenerContainer listenerContainer = new SimpleMessageListenerContainer();
				listenerContainer.setConnectionFactory(connectionFactory);
				listenerContainer.setQueues(consumerQueue());
				listenerContainer.setMessageListener(listener);
				listenerContainer.setAcknowledgeMode(AcknowledgeMode.AUTO);
				return listenerContainer;
		}
}
