package com.falcon;

import com.couchbase.client.java.document.RawJsonDocument;
import com.falcon.config.RawJsonDocumentSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableAutoConfiguration
@EnableSwagger2
public class Application
{
		public static void main(String[] args)
		{
				new SpringApplicationBuilder()
						.sources(Application.class)
						.properties("spring.config.name=application,messaging")
						.bannerMode(Banner.Mode.OFF)
						.run(args);
		}

		@Bean
		public ObjectMapper objectMapper()
		{
				final ObjectMapper objectMapper = new ObjectMapper();
				SimpleModule module = new SimpleModule();
				module.addSerializer(RawJsonDocument.class, new RawJsonDocumentSerializer());
				objectMapper.registerModule(module);
				return objectMapper;
		}

}
